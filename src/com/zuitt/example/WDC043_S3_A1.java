package com.zuitt.example;

import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args) {
        Scanner numbers = new Scanner(System.in);

        int factorial = 1;
        int num = 0;
        boolean isValid = false;

        while(!isValid) {
            try {
                System.out.println("Input an integer whose factorial will be computed:");
                num = numbers.nextInt();

                if (num < 0) {
                    System.out.println("Input must be a positive number!");
                }
                else {
                    isValid = true;
                }

            } catch (Exception e) {
                System.out.println("Invalid Input!");
                e.printStackTrace();
                numbers.next();
            }
        }

                for (int i = 1; i <= num; i++) {
                    factorial *= i;
                }

                System.out.println("The factorial of " + num + " is: " + factorial);

    }
}
